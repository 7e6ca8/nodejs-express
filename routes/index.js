var express = require('express');
var router = express.Router();
const util = require('util');
const process = require('process');
const childProcess = require('child_process');
const exec = util.promisify(childProcess.exec);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/df', async function(req, res, next) {
  const ls = await exec('ls -lah /tmp')
  console.log(ls.stdout)
  res.end(ls.stdout)
})

router.post('/df', async function(req, res, next) {
  const yes = childProcess.exec(`cat /dev/zero > /tmp/${new Date().getTime()}`)
  setTimeout(() => process.kill(yes.kill), 1000 * 60 * 5)
  res.end()
})

module.exports = router;
